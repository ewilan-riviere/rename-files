#!/usr/bin/env python3

"""Renaming class"""
import os
import sys
import glob
from typing import Callable
import re
import click
from unidecode import unidecode
import src.environment as env


class Renaming:
    """Rename files"""

    def __init__(self, inputable: bool = False):
        self.inputable = inputable
        self.print_python_version()

        print("This program will parse filenames to update filenames.")

    def replace_directories(self):
        """Replace name in dirs."""
        if self.confirm("\nDo you want to see a preview?", True):
            self.parse_directories(self.clean_file)
        if self.confirm("\nDo you want to update dirs?", True):
            self.parse_directories(self.clean_file_checked)
        print("Done")

    def replace_names(self):
        """Replace name in file."""
        if self.confirm("\nDo you want to see a preview?", True):
            self.parse_files(self.clean_file)
        if self.confirm("\nDo you want to update filenames?", True):
            self.parse_files(self.clean_file_checked)
        print("Done")

    def replace_names_youtube(self):
        """Replace name in file."""
        self.parse_files(self.clean_file_youtube)

    def replace_names_input(self):
        """Replace name in file."""
        self.parse_files_input()

    def clean_file_youtube(self, base_path: str, filename: str):
        """Clean filename."""
        old_file = os.path.join(base_path, filename)
        new_file = os.path.join(base_path, self.clean_string_youtube(filename))
        new_file = os.path.join(base_path, self.clean_string(new_file))
        os.rename(old_file, new_file)

        return ""

    def clean_file(self, base_path: str, filename: str):
        """Clean filename."""
        old_file = os.path.join(base_path, filename)
        new_file = os.path.join(base_path, self.clean_string(filename))
        print(f"\nold_file: {old_file}")
        print(f"new_file: {new_file}")

        return new_file

    def clean_file_checked(self, base_path: str, filename: str):
        """Clean filename."""
        old_file = os.path.join(base_path, filename)
        new_file = os.path.join(base_path, self.clean_string(filename))
        os.rename(old_file, new_file)

        return new_file

    # click.confirm('\nDo you want to see a preview?', default=True)
    def confirm(self, question: str, default: bool = True) -> bool:
        """Confirm question."""
        if default:
            return input(f"{question} [Y/n] ").lower() != "n"
        else:
            return input(f"{question} [y/N] ").lower() == "y"

    def confirm_click(self, _callback: Callable[[], bool]):
        """Ask user for yes or no."""
        if click.confirm("\nDo you want to continue?", default=True):
            print("Do something")
            _callback()

    def parse_directories(self, _callback: Callable[[str, str], str]):
        """Parse directories (skip files)."""
        dotenv = env.Environment()
        for path in glob.iglob(dotenv.directory + "**/**", recursive=True):
            if os.path.isdir(path):
                extension = os.path.splitext(path)[1]
                if extension != ".py":
                    base_path = os.path.dirname(path)
                    filename = os.path.basename(path)
                    _callback(base_path, filename)

    def parse_files(self, _callback: Callable[[str, str], str]):
        """Parse filenames (skip directories)."""
        dotenv = env.Environment()
        for path in glob.iglob(dotenv.directory + "**/**", recursive=True):
            if os.path.isfile(path):
                extension = os.path.splitext(path)[1]
                if extension != ".py":
                    base_path = os.path.dirname(path)
                    filename = os.path.basename(path)
                    _callback(base_path, filename)

    def parse_files_input(self):
        """Parse filenames (skip directories)."""
        from_value = input("From value?\n")
        to_value = input("To value?\n")

        dotenv = env.Environment()
        for path in glob.iglob(dotenv.directory + "**/**", recursive=True):
            if os.path.basename(path) == ".DS_Store":
                os.remove(path)
            if os.path.isfile(path):
                extension = os.path.splitext(path)[1]
                if extension != ".py":
                    base_path = os.path.dirname(path)
                    filename = os.path.basename(path)

                    old_file = os.path.join(base_path, filename)
                    new_file = old_file.replace(from_value, to_value)
                    os.rename(old_file, new_file)

    def clean_string_youtube(self, original: str) -> str:
        """Set string with special characters to unicode string."""
        name = self.clean_string(original)
        new_name = re.sub("\[.*?\]", "", name)

        return new_name

    def clean_string(self, original: str) -> str:
        """Set string with special characters to unicode string."""
        new_name = unidecode(original)
        new_name = new_name.replace(" - ", ".")
        new_name = new_name.replace("-", ".")
        new_name = re.sub("[^A-Za-z0-9.'/[/]]+", ".", new_name)
        new_name = re.sub("'", "", new_name)
        new_name = re.sub(" ", ".", new_name)
        new_name = re.sub("[!@#$%^&*`,]", "", new_name)
        new_name = new_name.replace(":", "")
        new_name = re.sub("[.]{2,}", ".", new_name)
        # new_name = re.sub("/", '', new_name)
        # new_name = re.sub(".:.", '.', new_name)

        return new_name

    def print_python_version(self):
        """Print python version."""
        print(sys.version)
