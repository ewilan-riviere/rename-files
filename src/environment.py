"""Environment"""
import os
from dotenv import load_dotenv

load_dotenv()


class Environment:
    """Environment"""

    def __init__(self):
        self.directory = os.getenv('DIRECTORY') or os.getcwd()
