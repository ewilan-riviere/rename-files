#!/usr/bin/env python3

"""Deleting class"""
import os
import stat
import glob
import src.environment as env


class Deleting:
    """Deleting files"""

    def __init__(self):
        print('This program will delete files.')
        self.parse_files()

    def has_hidden_attribute(self, filepath: str):
        """Check if file has hidden attribute."""
        return bool(os.stat(filepath).st_file_attributes
                    & stat.FILE_ATTRIBUTE_HIDDEN)

    def parse_files(self):
        """Parse filenames(skip directories)."""
        dotenv = env.Environment()

        ext_to_delete = ['vsmeta', 'DS_Store', 'ini', 'db']

        for path in glob.iglob(dotenv.directory + '**/**', recursive=True):
            if os.path.isfile(path):
                ext = os.path.splitext(path)[1]
                if ext in ext_to_delete:
                    if self.has_hidden_attribute(path):
                        print(f'Hidden: {path}')
                    else:
                        print(f'Not hidden: {path}')
                        os.remove(path)

        for root, dirs, files in os.walk(dotenv.directory, topdown=False):
            i = 0
            for file in files:
                if file.endswith('.DS_Store'):
                    path = os.path.join(root, file)
                    os.remove(path)
