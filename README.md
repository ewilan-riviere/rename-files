# Rename Files

This is a simple script to rename files in a directory.

## Install

```bash
pip3 install click unidecode python-dotenv
```

```bash
cp .env.example .env
```

## Usage

```bash
python3 .\main.py
```

```bash
python3 .\input.py
```

```bash
python3 .\delete.py
```

```bash
python3 .\youtube.py
```
