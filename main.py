#!/usr/bin/env python3
"""Init scripts."""

import sys

import src.renaming as rename
import src.deleting as delete

sys.path.append("./src")

delete.Deleting()
rename.Renaming().replace_directories()
rename.Renaming().replace_names()
